//get all elements
const loanElement = document.getElementById("loan");
const textLoanElement = document.getElementById("textToLoan");
const loanBtnElement = document.getElementById("loanButton");
const workBtnElement = document.getElementById("workButton");
const bankBtnElement = document.getElementById("bankButton");
const laptopsElement = document.getElementById("laptops");
const featureElement = document.getElementById("features");
const laptopHeadElement = document.getElementById("laptopHead");
const laptopImgElement = document.getElementById("laptopImg");
const laptopParagrafElement = document.getElementById("laptopParagraf");
const laptopPriceElement = document.getElementById("laptopPrice");
const buyLaptopBtnElement = document.getElementById("buyButton");
let balanceElement = document.getElementById("balance");
let payElement = document.getElementById("pay");

//Creating elements
let repayLoanBtn = document.createElement("button");
repayLoanBtn.innerText = "Repay loan";
repayLoanBtn.classList.add("repayButton");
var img = document.createElement("img");
let buyLaptopBtn = document.createElement("button");
buyLaptopBtn.innerText = "BUY NOW";
buyLaptopBtn.classList.add("buyButton");

//Adding eventlisteners
loanBtnElement.addEventListener("click", getALoan);
workBtnElement.addEventListener("click", addWork);
bankBtnElement.addEventListener("click", transferToBank);
repayLoanBtn.addEventListener("click", repayLoan);
buyLaptopBtn.addEventListener("click", buyLaptop);

// unnessesary Variables :( but in use
let balanceValue = balanceElement.innerText;
let payValue = payElement.innerText;

//adding the button to buy right away
addBuyButton();

//Function to get a loan
function getALoan(){
    let amount=prompt("Enter amount to loan");
    //If promt is empty or have letters or cancel is pressed
    if(amount == "" || isNaN(amount) || amount == null){
        alert("Only numbers");
    }
    //If you have a loan
    else if(parseInt(loanElement.innerText)>0){
        alert("No loan until you repayed your loan");
    }
    //If the amount is to much
    else if((balanceElement.innerText*2)<amount){
        alert("the amount is to much, you can not get a loan");
    }
    //get the loan
    else{
        balanceValue = parseInt(balanceElement.innerText) + parseInt(amount);
        console.log(balanceElement.innerText);
        document.getElementById("balance").innerText = balanceValue;
        textLoanElement.innerText="Loan: ";
        loanElement.innerText=amount;
        addRepayButton();
        alert(`Your loan is now: ${loanElement.innerText}`);
    }
}
//Function to increase your Pay balance by 100 on each click
function addWork(){
    payValue = parseInt(payValue) + 100;
    document.getElementById("pay").innerText = payValue;
    //console.log(payValue);
}
//Function to transfer pay to bank
function transferToBank(){
    //if there is not any money in pay account
    if(payValue<=0){
        alert("work to get money");
    }
    //if you have a loan
    if(checkIfLoan()==true){
        transferToBankAndLoan();
    }
    //If you dont have a loan transfer all pay to bank
    else if(checkIfLoan()==false){
        balanceValue = parseInt(balanceElement.innerText) + parseInt(payElement.innerText);
        document.getElementById("balance").innerText = balanceValue;
    }
    payValue = 0;
    document.getElementById("pay").innerText = payValue;
}
//Function to check if there is a loan
function checkIfLoan(){
    let haveLoan = false;
    if(parseInt(loanElement.innerText)>0){
        //you have a loan
        haveLoan = true;
    }
    else{
        //you have no loan
        haveLoan = false;
    }
    //return boolean
    return haveLoan;
}
//function to add button to repay loan
function addRepayButton(){
    //placing it in div repay button
    document.getElementById("repayButton").appendChild(repayLoanBtn);
}
//function to remove button to repay loan
function removeRepayButton(){
    document.getElementById("repayButton").removeChild(repayLoanBtn);
}
//Function to add button to buy //Could just have made it in html
function addBuyButton(){
    buyLaptopBtnElement.appendChild(buyLaptopBtn);
}
//function to place money in bank and repay loan
function transferToBankAndLoan(){
    let amountToLoan = 0;
    let amountToBank = 0;
    amountToLoan = parseInt(payValue) * 0.1;
    amountToBank = parseInt(payValue) - amountToLoan;
    if(amountToLoan>loanElement.innerText){
        parseInt(payValue) - parseInt(loanElement.innerText);
    }
    //to not over pay the loan 
    if(loanElement.innerText<=amountToLoan){
        let restOfAmountToLoan = 0;
        restOfAmountToLoan = parseInt(payValue) - parseInt(loanElement.innerText)
        balanceValue = parseInt(balanceValue) + restOfAmountToLoan;
        document.getElementById("loan").innerText = 0;
        document.getElementById("balance").innerText = balanceValue;
        removeRepayButton();
        alert("Full loan repaid");
        textLoanElement.innerText="";
        loanElement.innerText="";
    }
    //Repay
    else if(parseInt(loanElement.innerText)>amountToLoan){
        balanceValue = parseInt(balanceValue) + amountToBank;
        loanElement.innerText = parseInt(loanElement.innerText) - amountToLoan;
        document.getElementById("balance").innerText = balanceValue;
        document.getElementById("loan").innerText = loanElement.innerText;
    }
}
//Function to repay loan with full pay 
//leaves the rest of pay in payment account
function repayLoan(){
    let valueToRepay = 0;
    //Repay full loan
    if(loanElement.innerText<=parseInt(payValue)){ 
        payValue = parseInt(payValue) - parseInt(loanElement.innerText);
        document.getElementById("pay").innerText = payValue;
        document.getElementById("loan").innerText = 0; //Testa att räkna ut vad det blir istället
        removeRepayButton();
        alert("Full loan repaid");
        textLoanElement.innerText="";
        loanElement.innerText="";
    }
    //repay part of loan
    else if(loanElement.innerText>parseInt(payValue)){
        console.log("lånet är större än lönen")
        console.log(payValue);
        //betala av så mycket det går på lånet, lämna det med resterande värde
        loanElement.innerText = parseInt(loanElement.innerText) - parseInt(payValue);
        document.getElementById("loan").innerText = loanElement.innerText;
        payValue = 0;
        document.getElementById("pay").innerText = payValue;
        console.log(payValue);
    }
}
let liElement = ""; 
let laptops = [];
fetch("https://noroff-komputer-store-api.herokuapp.com/computers")
    .then(response => response.json())
    .then(data => laptops = data)
    .then(laptops => addLaptopsToMenu(laptops));

    //HÄÄÄÄÄR
const addLaptopsToMenu=(laptops) => {
    laptops.forEach(x => addLaptopToMenu(x));
    //Added from start
    img.src = "https://noroff-komputer-store-api.herokuapp.com/" + laptops[0].image;
    laptopImgElement.appendChild(img); 
    laptopHeadElement.innerText = laptops[0].title;
    laptopParagrafElement.innerText = laptops[0].description;
    laptopPriceElement.innerText = laptops[0].price + " Kr";
    for(k of laptops[0].specs){
        addLaptopsSpecsToLi(laptops[0]) 
    }
}
//Adding laptops to element
const addLaptopToMenu = (laptop) => {
    const laptopElement = document.createElement("option");
    laptopElement.value = laptop.id;
    laptopElement.appendChild(document.createTextNode(laptop.title));
    laptopsElement.appendChild(laptopElement);
}
//Adding laptops specs to element
const addLaptopsSpecsToLi = (laptop) => {  
    liElement = document.createElement("li");
    liElement.value = laptop.id;
    liElement.appendChild(document.createTextNode(k));
    liElement.classList.add("specsList");
    featureElement.appendChild(liElement);
}

//for the changes when selecting laptop
const handleLaptopMenu = e => {
    document.getElementById("features").innerHTML = '';
    const selectedLaptop = laptops[e.target.selectedIndex]; 
    for(k of selectedLaptop.specs){
        addLaptopsSpecsToLi(selectedLaptop);
    }  
    img.src = "https://noroff-komputer-store-api.herokuapp.com/" + selectedLaptop.image;
    //just to save the broken picture
    if(selectedLaptop.id==5){
        img.src = "https://noroff-komputer-store-api.herokuapp.com/assets/images/5.png" 
    }
    laptopImgElement.appendChild(img); 
    laptopHeadElement.innerText = selectedLaptop.title;
    laptopParagrafElement.innerText = selectedLaptop.description;
    laptopPriceElement.innerText = selectedLaptop.price + " Kr";
}

//Adding eventlistener to selectlist
laptopsElement.addEventListener("change", handleLaptopMenu);

//function to buy a lapbop
function buyLaptop(){
    const selectedtest = laptops[laptopsElement.selectedIndex];
    console.log(selectedtest.title);
    laptops.forEach(element => {
        if(element.title==selectedtest.title){
            //afford
            if(parseInt(balanceValue)>element.price){
                balanceValue = parseInt(balanceValue) - parseInt(element.price);
                document.getElementById("balance").innerText = balanceValue;
                alert(`Congratulations to your new laptop: ${element.title}`)
            }
            //not afford
            else if (parseInt(balanceValue)<element.price){
                alert("You can not afford the laptop");
            }
        }
    });
}
